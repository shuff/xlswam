Xls Wam
=======

This was the beginning implementation of a Warren Abstract Machine (WAM) using XLS.  This was to serve as a teaching aid to myself to see how memory and registers of the VM changed visually as each instruction was executed.  Naturally it wasn't implemented to be fast, and it isn't, but it simulates an instruction-level debugger interface by using Excel's conditional formatting to render current instruction pointer location in the instruction stream.

I followed along the paper Warren’s Abstract Machine A TUTORIAL RECONSTRUCTION by HASSAN AIT-KACI February 18, 1999.  This WAM currently functions in executing a number of the instructions.  A parser was not yet implemented for converting the Prolog clauses into byte code.  It's far from complete.  Only about 8 hours that I recall was spent on it initially while I was supposed to be doing real work... nitpicked it after.
